package org.prime;

import java.util.Scanner;

public class CheckPrime {
	
	public static Scanner scanner;

	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		
		System.out.print("Insert the fist number: ");
		int firstNo = scanner.nextInt();
		
		System.out.print("Insert the second number: ");
		int secondNo = scanner.nextInt();
		
		System.out.println("There are " + checkInterval(firstNo, secondNo) + " prime numbers in the [" + firstNo + "," + secondNo + "] interval");
		
	}
	
	public static boolean isPrime(int number) {
		if (number < 2) {
			return false;
		}
		
		for (int i = 2; i <= number/2; i++) {
			if (number % i == 0 ) {
				return false;
			}
		}
		return true;
	}
	
	public static int checkInterval (int firstNo, int secondNo) {
		int contor = 0;
		for (int i = firstNo; i < secondNo; i++) {
			if (isPrime(i)) {
				contor ++;
			}
		}
		return contor;
	}

}
